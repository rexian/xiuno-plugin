<?php
!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
//$sql = "DROP TABLE IF EXISTS `{$tablepre}collection_node`;";
//$r = db_exec($sql);
$sql = "CREATE TABLE IF NOT EXISTS `{$tablepre}collection_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `lastdate` int(10) unsigned NOT NULL DEFAULT '0',
  `sourcecharset` varchar(8) NOT NULL,
  `sourcetype` varchar(20) NOT NULL DEFAULT '0',
  `urlpage` text NOT NULL,
  `pagesize_start` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `pagesize_end` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `page_base` char(255) NOT NULL,
  `par_num` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `url_contain` char(100) NOT NULL,
  `url_except` char(100) NOT NULL,
  `url_start` char(100) NOT NULL DEFAULT '',
  `url_end` char(100) NOT NULL DEFAULT '',
  `title_rule` char(100) NOT NULL,
  `title_html_rule` text NOT NULL,
  `content_rule` char(100) NOT NULL,
  `content_html_rule` text NOT NULL,
  `content_page_start` char(100) NOT NULL,
  `content_page_end` char(100) NOT NULL,
  `content_page_rule` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `content_page` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `content_nextpage` char(100) NOT NULL,
  `down_attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `watermark` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `watermark_img` varchar(150) NOT NULL DEFAULT '',
  `watermark_img_w` smallint(5) NOT NULL DEFAULT '0',
  `watermark_img_h` smallint(5) NOT NULL DEFAULT '0',
  `coll_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `customize_config` text NOT NULL,
  `post_uids` varchar(100) NOT NULL DEFAULT '',
  `post_times` varchar(100) NOT NULL DEFAULT '',
  `time_post_is` tinyint(1) NOT NULL DEFAULT '0',
  `time_post_url` tinyint(1) NOT NULL DEFAULT '0',
  `time_post_times` varchar(255) NOT NULL DEFAULT '',
  `time_post_forumids` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$r = db_exec($sql);

//$sql = "DROP TABLE IF EXISTS `{$tablepre}collection_history`;";
//$r = db_exec($sql);
$sql = "CREATE TABLE IF NOT EXISTS  `{$tablepre}collection_history` (
  `md5url` char(32) NOT NULL,
  PRIMARY KEY (`md5url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$r = db_exec($sql);

//$sql = "DROP TABLE IF EXISTS `{$tablepre}collection_content`;";
//$r = db_exec($sql);
$sql = "CREATE TABLE IF NOT EXISTS `{$tablepre}collection_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nodeid` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `url` char(255) NOT NULL,
  `title` char(100) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nodeid` (`nodeid`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

$r = db_exec($sql);

//$sql = "DROP TABLE IF EXISTS `{$tablepre}collection_crontab_log`;";
//$r = db_exec($sql);
$sql = "CREATE TABLE IF NOT EXISTS `{$tablepre}collection_crontab_log` (
   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `day_date` int(10) NOT NULL,
  `is_post` tinyint(1) NOT NULL DEFAULT '0',
  `post_number` smallint(6) NOT NULL DEFAULT '0',
  `node_id` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `day_date` (`day_date`,`node_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

$r = db_exec($sql);

$sql = "CREATE TABLE IF NOT EXISTS `{$tablepre}collection_attach` (
  `aid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL DEFAULT '0',
  `filesize` int(8) unsigned NOT NULL DEFAULT '0',
  `width` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `height` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `filename` char(120) NOT NULL DEFAULT '',
  `orgfilename` char(120) NOT NULL DEFAULT '',
  `filetype` char(7) NOT NULL DEFAULT '',
  `create_date` int(11) unsigned NOT NULL DEFAULT '0',
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  KEY `content_id` (`content_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$r = db_exec($sql);
?>