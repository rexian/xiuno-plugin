<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

// ------------> 最原生的 CURD，无关联其他数据。

function collection_content__create($arr) {
    $r = db_create('collection_content', $arr);
    return $r;
}

function collection_content__update($id, $arr) {
    $r = db_update('collection_content', array('id'=>$id), $arr);
    return $r;
}

function collection_content__read($id) {
    $data = db_find_one('collection_content', array('id'=>$id));
    return $data;
}

function collection_content__delete($id) {
    $r = db_delete('collection_content', array('id'=>$id));
    return $r;
}

function collection_content__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 20) {
    $lists = db_find('collection_content', $cond, $orderby, $page, $pagesize);
    return $lists;
}


function collection_content_count($cond = array()) {
    $n = db_count('collection_content', $cond);
    return $n;
}

?>