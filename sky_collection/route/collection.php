<?php
!defined('DEBUG') AND exit('Access Denied.');
error_reporting(0);
/**
 * 采集管理
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

include APP_PATH . 'plugin/sky_collection/lib/func.php';

$action = param(1);

if (empty($action) || $action == 'list') {
    if ($method == 'GET') {
        $pagesize = 10;
        $page = param(2, 1);
        $cond = array();
        $n = collection_node_count($cond);
        $lists = collection_node__find($cond, array('id' => -1), $page, $pagesize);
        $pagination = pagination(url("collection-list-{page}"), $n, $page, $pagesize);
        include _include(APP_PATH . "plugin/sky_collection/view/htm/collection_node_list.htm");
    }
} else if ($action == 'add') {
    $forums = forum__find();
    if ($method == 'GET') {
        include _include(APP_PATH . "plugin/sky_collection/view/htm/collection_node_add.htm");
    } else {
        $data = $_POST;
        empty($data['name']) AND message('name', '采集名称不能为空');
        //$data['time_post_times'] = strtotime($data['time_post_times']);
        $data['time_post_forumids'] = isset($data['time_post_forumids']) ? json_encode($data['time_post_forumids']): '';
        $data['urlpage'] = isset($data[$data['sourcetype']]) ? $data[$data['sourcetype']] : message('sourcetype', '请选择网址类型');
        unset($data['urlpage1'], $data['urlpage2'], $data['urlpage3']);
        $res = collection_node_read_cond(array('name'=>$data['name']));
        $res && message(-1, '采集名称已存在<script>setTimeout(function(){history.go(-1);},1000)</script>');
        collection_node__create($data);
        message(0, '添加成功<script>setTimeout(function(){history.go(-1);},1000)</script>');
    }
} else if($action == 'update') {
    $id = param(2);
    $forums = forum__find();
    if ($method == 'GET') {
        $node = collection_node__read($id);
        if($node['time_post_forumids']) {
            $time_post_forumids = json_decode($node['time_post_forumids'], true);
            if($time_post_forumids) {
                $node['time_post_forumids'] = array();
                foreach ($time_post_forumids as $val) {
                    $node['time_post_forumids'][$val] = $val;
                }
            }
        }
        empty($node) && message(-1, '采集节点不存在<script>setTimeout(function(){history.go(-1);},1000)</script>');
        include _include(APP_PATH . "plugin/sky_collection/view/htm/collection_node_update.htm");
    } else {
        $data = $_POST;
        empty($data['name']) AND message('name', '采集名称不能为空');
        $data['urlpage'] = isset($data[$data['sourcetype']]) ? $data[$data['sourcetype']] : message('sourcetype', '请选择网址类型');
        unset($data['urlpage1'], $data['urlpage2'], $data['urlpage3']);
        //$data['time_post_times'] = strtotime($data['time_post_times']);
        $data['time_post_forumids'] = isset($data['time_post_forumids']) ? json_encode($data['time_post_forumids']): '';
        collection_node__update($id, $data);
        message(0, '修改成功<script>setTimeout(function(){history.go(-1);},1000)</script>');
    }
} else if($action == 'delete') {
    $id = param(2);
    collection_node__delete($id);
    message(0, '删除成功<script>setTimeout(function(){history.go(-1);},1000)</script>');
} else if ($action == 'urls') {
    $id = param(2);
    $page = param(3, 1);
    $data = collection_node__read($id);
    empty($data) && message(-1, '采集节点不存在<script>setTimeout(function(){history.go(-1);},1000)</script>');
    $urls = url_list($data);
    $total_page = count($urls);
    if ($total_page > 0) {
        $url_list = $urls[$page];
        $url = get_url_lists($url_list, $data);
        $total = count($url);
        $re = 0;
        if (is_array($url) && !empty($url)) foreach ($url as $v) {
            if (empty($v['url']) || empty($v['title'])) continue;
            $v = new_addslashes($v);
            $v['title'] = strip_tags($v['title']);
            $md5 = md5($v['url']);
            if (!collection_history__read($md5)) {
                collection_history__create(array('md5url' => $md5));
                collection_content__create(array('nodeid' => $id, 'status' => 0, 'url' => $v['url'], 'title' => $v['title']));
            } else {
                $re++;
            }
        }
        if ($total_page <= $page) {
            collection_node__update($id, array('lastdate' => time()));
        }
        include _include(APP_PATH . "plugin/sky_collection/view/htm/urls.htm");
    }
} else if ($action == 'contents') {
    $id = param(2);
    $page = param(3, 1);
    $total = param(4, 0);
    $data = collection_node__read($id);
    empty($data) && message(-1, '采集节点不存在');
    if (empty($total)) $total = collection_content_count(array('nodeid' => $id, 'status' => 0));
    $total_page = ceil($total / 2);
    $list = collection_content__find(array('nodeid' => $id, 'status' => 0), array('id'=>-1), 1, 2);
    $i = 0;
    if (!empty($list) && is_array($list)) {
        foreach ($list as $v) {
            unset($GLOBALS['collection_content']);
            $GLOBALS['collection_content'] = $v;
            $html = get_content($v['url'], $data);
            collection_content__update($v['id'], array('status' => 1, 'data' => json_encode($html)));
            $i++;
        }
        unset($GLOBALS['collection_content']);
    } else {
        message(-1, '已全部采集完成<script>setTimeout(function(){history.go(-1);},1000)</script>');
    }
    if ($total_page <= $page) {
        collection_node__update($id, array('lastdate' => time()));
    }
    include _include(APP_PATH . "plugin/sky_collection/view/htm/cotnent.htm");
} else if($action == 'publist') {
    $id = param(2);
    $page = param(3, 1);
    $status = param(4);
    $statusCur = '';
    if ($method == 'GET') {
        $pagesize = 10;
        $cond = array('nodeid'=>$id);
        if($status !== '') {
            $cond['status'] = $status;
            $statusCur = (int)$status;
        }
        $n = collection_content_count($cond);
        $lists = collection_content__find($cond, array('id' => -1), $page, $pagesize);
        $pagination = pagination(url("collection-publist-{$id}-{page}-{$status}"), $n, $page, $pagesize);
        $forums = forum__find();
        include _include(APP_PATH . "plugin/sky_collection/view/htm/publist.htm");
    }
} else if($action == 'optionall') {
    if ($method == 'POST') {
        $data = $_POST;
        if($data['option'] != 'importall') {
            empty($data['ids']) && message(-1, '请选择要操作的数据<script>setTimeout(function(){history.go(-1);},1000)</script>');
        }
        empty($data['option']) && message(-1, '请选择操作<script>setTimeout(function(){history.go(-1);},1000)</script>');
        if($data['option'] == 'delete') {
            foreach ($data['ids'] as $id) {
                collection_content__delete($id);
                collection_attach__delete_cond(array('content_id'=>$id));
            }
        } else if($data['option'] == 'deleteall') {
            foreach ($data['ids'] as $id) {
                $data = collection_content__read($id);
                collection_history__delete(md5($data['url']));
                collection_content__delete($id);
                collection_attach__delete_cond(array('content_id'=>$id));
            }
        } else if($data['option'] == 'import') {
            empty($data['forumids']) && message(-1, '请选择要导入的版块<script>setTimeout(function(){history.go(-1);},1000)</script>');
            sky_import_data($data['nodeid'], $data['forumids'], $data['ids']);

        } else if($data['option'] == 'importall') {
            sky_import_data($data['nodeid'], $data['forumids']);
        }
        message(0, '操作成功<script>setTimeout(function(){history.go(-1);},1000)</script>');
    }
} else if($action == 'publictest') {
    $id = param(2);
    $data = collection_node__read($id);
    if($data) {
        $urls = url_list($data, 1);
        if (!empty($urls)) foreach ($urls as $v) {
            $tmpurl = get_url_lists($v, $data);
        }
        foreach ($tmpurl as $val) {
            $url[$val['url']] = $val;
        }
        include _include(APP_PATH . "plugin/sky_collection/view/htm/publictest.htm");
    }
} else if($action == 'publictestcontent') {
    $url = param('url');
    $id = param('id');
    $data = collection_node__read($id);
    if($url && $data) {
        $contents = get_content($url, $data);
        foreach ($contents as $_key=>$_content) {
            if(trim($_content)=='') $contents[$_key] = '没有获取到内容，检查一下规则吧^_^';
        }
        $contents['content'] = htmlentities($contents['content']);
        echo '<pre>';
        print_r($contents);
        echo '</pre>';
    }
} else if ($action == 'copydata') {
    $id = param('id');
    $name = param('name');
    empty($name) && message(-1, '请填写采集名称<script>setTimeout(function(){history.go(-1);},1000)</script>');
    $data = collection_node__read($id);
    if($data) {
        $res = collection_node_read_cond(array('name'=>$name));
        $res && message(-1, '采集名称已存在<script>setTimeout(function(){history.go(-1);},1000)</script>');
        $data['name'] = $name;
        unset($data['id']);
        collection_node__create($data);
        message(0, '复制成功<script>setTimeout(function(){history.go(-1);},1000)</script>');
    }
}

?>

