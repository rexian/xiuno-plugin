<?php

/*
	Xiuno BBS 4.0 大白_笔记模板
*/

!defined('DEBUG') AND exit('Forbidden');


// 删除数据就没有了
setting_delete('huux_style_one');


// 从菜单中移除
$kv = kv_get('dabai_plugin');
if ($kv){
	unset($kv['huux_style_one']);
	kv_set('dabai_plugin', $kv);
}else{
	message(-1, '安装时出错,已经删除。');
}

?>